# On-Call Handover

- EOC egress: @
- EOC ingress: @
/assign @

## Summary:

#### What (if any) time-critical work is being handed over?

Please list any urgent tasks that cannot wait, such as mitigating an active incident
or preventing imminent breakage.

#### What contextual info may be useful for the next few on-call shifts?

Please help the incoming on-callers to efficiently interpret symptoms or alerts.  For example:
* Are any system components known to be in an abnormal, degraded, or risky state?
* Do you think any recent problems may recurr or have delayed or lingering side effects?
* If a significant incident occurred, can you briefly summarize its current status?

## Ongoing alerts/incidents:

- 

## Resolved actionable alerts:

- 

## Unactionable alerts:

-

## Resolved production incidents:

-

## Change issues:

-

<!-- 
- Please set the  issue title to "On-Call Handover YYYY-MM-DD HH:MM UTC"
- Tag yourself as the EOC egress
- Tag the next on-call as the EOC Ingress
- Please do not forget to close and link the previous report as a related issue
- If there are any ongoing incidents, please also directly reach out to the next on-call 
-->

/label ~"SRE:On-call"
